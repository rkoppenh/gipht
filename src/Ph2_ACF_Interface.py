from PySide2.QtCore import Signal, QObject
import sys, os

class Ph2_ACF_Interface(QObject):

    update = Signal ( object , object )

    def __init__( self, pPh2AcfFolder, pType, pIndex ):
        super(Ph2_ACF_Interface, self).__init__(  )
        self.ph2AcfFolder   = pPh2AcfFolder
        self.index = pIndex

        try:
            import pythonUtils.Ph2_ACF_StateMachine as Ph2_ACF_StateMachine
            import lib.Ph2_ACF_PythonInterface as Ph2_ACF
            sys.path.insert(1, os.getenv('PH2ACF_BASE_DIR'))
            Ph2_ACF.configureLogger(os.getenv('PH2ACF_BASE_DIR') + "/settings/logger.conf")
            self.sm = Ph2_ACF_StateMachine.StateMachine()
        except ModuleNotFoundError:
            print("Ph2_ACF nor reachable")
        #self.logFile = open("log/log" + str(self.index) + ".txt", "w")

    def listFirmware( self, pConfig, pBoard):
        try:
            firmwareList = self.sm.listFirmware(pConfig,pBoard)
            for firmware in firmwareList:
                self.update.emit("data","Firmware>"+firmware)
        except:
            print("FW list empty")
    def loadFirmware(self, pConfig, pName, pBoard):

        self.sm.loadFirmware(pConfig,pName,pBoard)
        if self.sm.isSuccess():
            self.update.emit("data", "NewFirmware>"+pName)

    def uploadFirmware(self, pConfig, pFirmware, pBoard):
        firmwarePath = "./Bitstreams/" + pFirmware
        self.sm.uploadFirmware(pConfig, pFirmware, firmwarePath, pBoard)
        if self.sm.isSuccess():
            self.update.emit("data", "NewFirmware>"+pFirmware)

    def moduleTest(self, pConfig, pBoard, pResultDirectory ,pRunNumber, pSelectedMeasurements):
        #Resultsdirectory must be added in python ph2acf
        try:
            import pythonUtils.Ph2_ACF_StateMachine as Ph2_ACF_StateMachine
            import lib.Ph2_ACF_PythonInterface as Ph2_ACF
            sys.path.insert(1, os.getenv('PH2ACF_BASE_DIR'))

            Ph2_ACF.configureLogger(os.getenv('PH2ACF_BASE_DIR') + "/settings/logger.conf")
            self.sm = Ph2_ACF_StateMachine.StateMachine()
        except ModuleNotFoundError:
            print("Ph2_ACF nor reachable")

        calibrationname = "calibration"
        if "Noise" in pSelectedMeasurements:
            calibrationname += "andpedenoise"
        if "KIRA" in pSelectedMeasurements:
            calibrationname += "andkira"

        self.sm.setCalibrationName(calibrationname)
        self.sm.setConfigurationFile(pConfig)

        os.environ['GIPHT_RESULT_FOLDER'] = pResultDirectory
        self.sm.setRunNumber(pRunNumber)
        self.sm.runCalibration()
        self.update.emit("finished",None)

